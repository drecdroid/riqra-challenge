# https://medium.com/trendyol-tech/how-we-reduce-node-docker-image-size-in-3-steps-ff2762b51d5a
FROM node:12.16.2-alpine AS BUILD_IMAGE

RUN apk update && apk add curl bash && rm -rf /var/cache/apk/*

# Client building
RUN mkdir -p /usr/src/app/client
WORKDIR /usr/src/app/client
COPY ./client/package.json ./client/yarn.lock ./
RUN yarn --frozen-lockfile
COPY ./client .
ENV NODE_ENV production
ENV GENERATE_SOURCEMAP=false
RUN yarn build

# Server slimming
RUN curl -sfL https://install.goreleaser.com/github.com/tj/node-prune.sh | bash -s -- -b /usr/local/bin

RUN mkdir -p /usr/src/app/server
WORKDIR /usr/src/app/server
COPY ./server/package.json ./server/yarn.lock ./
RUN yarn --frozen-lockfile
RUN npm prune --production
RUN /usr/local/bin/node-prune

FROM node:12.16.2-alpine

ARG PORT=${PORT}
ARG DB_USERNAME=${DB_USERNAME}
ARG DB_PASSWORD=${DB_PASSWORD}
ARG DB_NAME=${DB_NAME}
ARG DB_HOST=${DB_HOST}
ENV DB_PORT 3306
ENV NODE_ENV production

RUN mkdir -p /usr/src/app/client/build
WORKDIR /usr/src/app/client
COPY --from=BUILD_IMAGE /usr/src/app/client/build ./build

RUN mkdir -p /usr/src/app/server
WORKDIR /usr/src/app/server
COPY ./server .
COPY --from=BUILD_IMAGE /usr/src/app/server/node_modules ./node_modules

EXPOSE $PORT
RUN node resyncDb.js
CMD yarn start