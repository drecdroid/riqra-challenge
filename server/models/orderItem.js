'use strict';
module.exports = (sequelize, DataTypes) => {
  const OrderItem = sequelize.define('OrderItem', {
    quantity: DataTypes.INTEGER,
    productPrice: DataTypes.STRING,
    productTotal: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'OrderItem',
    tableName: 'OrderItems'
  });

  OrderItem.associate = function(models) {
    OrderItem.belongsTo(models['Product'], { foreignKey: 'productCode' })
    models['Order'].hasMany(OrderItem, { foreignKey: 'orderId', onDelete: 'CASCADE', as: 'orderItems' })
  };
  
  return OrderItem;
};