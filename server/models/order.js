'use strict';

module.exports = (sequelize, DataTypes) => {
  const Order = sequelize.define('Order', {
    total: DataTypes.STRING,
    taxes: DataTypes.STRING,
    subtotal: DataTypes.STRING,
    shipping: DataTypes.STRING,
    shippinDate: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'Order',
    tableName: 'Orders',
  });

  Order.associate = function(models) {
    Order.belongsTo(models['Session'], {
      foreignKey: 'sessionId',
      onDelete: 'cascade'
    })
  };
  
  return Order;
};