'use strict';

module.exports = (sequelize, DataTypes) => {
  const Product = sequelize.define('Product', {
    name: {
      type: DataTypes.STRING,
    },
    code: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    photoUrl: {
      type: DataTypes.STRING
    },
    price: {
      type: DataTypes.DECIMAL(19, 4)
    }
  }, {
    sequelize,
    modelName: 'Product',
    tableName: 'Products',
    indexes: [
      {
        name: 'name_index',
        using: 'BTREE',
        fields: ['name']
      }
    ]
  });

  
  return Product;
};