'use strict';
module.exports = (sequelize, DataTypes) => {
  const Cart = sequelize.define('Cart', {}, {
    sequelize,
    modelName: 'Cart',
    tableName: 'Carts'
  });

  Cart.associate = function(models) {
    Cart.belongsTo(models['Session'], {
      foreignKey: 'sessionId',
      onDelete: 'cascade'
    })
  };
  
  return Cart;
};