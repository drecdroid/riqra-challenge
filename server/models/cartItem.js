'use strict';
module.exports = (sequelize, DataTypes) => {
  const CartItem = sequelize.define('CartItem', {
    quantity: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'CartItem',
    tableName: 'CartItems'
  });

  CartItem.associate = function(models) {
    CartItem.belongsTo(models['Product'], { foreignKey: 'productCode', onDelete: 'CASCADE', as: 'product' })
    models['Cart'].hasMany(CartItem, { foreignKey: 'cartId', onDelete: 'CASCADE', as: 'cartItems' })
  };
  
  return CartItem;
};