const {
  GraphQLSchema,
  GraphQLObjectType,
  GraphQLBoolean,
  GraphQLList,
  GraphQLNonNull,
  GraphQLString,
  GraphQLID,
  GraphQLInt
} = require('graphql')

const {
  createProduct,
  searchProducts,
  addProductToCart,
  createSession,
  order,
  product,
  getCart,
  removeProductFromCart,
  incrementCartItem,
  decrementCartItem,
  createOrderFromCart
} = require('./resolvers')

const { GraphQLUpload } = require('graphql-upload')

const SessionType = new GraphQLObjectType({
  name: 'Session',
  fields: {
    sessionId: { type: GraphQLID },
    cartId: { type: GraphQLString }
  }
})

const ProductType = new GraphQLObjectType({
  name: 'Product',
  fields: {
    code: { type: GraphQLID },
    name: { type: GraphQLString },
    photoUrl: { type: GraphQLString },
    price: { type: GraphQLString }
  }
})

const OrderItemType = new GraphQLObjectType({
  name: 'OrderItem',
  fields: {
    id: { type: GraphQLID },
    productCode: { type: ProductType },
    price: { type: GraphQLString },
    quantity: { type: GraphQLInt }
  }
})

const OrderType = new GraphQLObjectType({
  name: 'Order',
  fields: {
    id: { type: GraphQLID },
    subtotal: { type: GraphQLString },
    taxes: { type: GraphQLString },
    shipping: { type: GraphQLString },
    total: { type: GraphQLString },
    shippingDate: { type: GraphQLString },
    orderItems: { type: GraphQLNonNull(GraphQLList(GraphQLNonNull(OrderItemType)))}
  }
})

const CartItemType = new GraphQLObjectType({
  name: 'CartItem',
  fields: {
    id: { type: GraphQLID },
    productCode: { type: GraphQLNonNull(GraphQLID) },
    quantity: { type: GraphQLInt }
  }
})

const CartType = new GraphQLObjectType({
  name: 'Cart',
  fields: {
    id: { type: GraphQLID },
    sessionId: { type: GraphQLNonNull(GraphQLID) },
    cartItems: { type: GraphQLNonNull(GraphQLList(GraphQLNonNull(CartItemType)))}
  }
})

const queryType = new GraphQLObjectType({
  name: 'Query',
  fields: {
    searchProducts: {
      type: GraphQLNonNull(GraphQLList(GraphQLNonNull(ProductType))),
      args: {
        like: { type: GraphQLNonNull(GraphQLString) }
      },
      resolve: searchProducts 
    },
    product: {
      type: ProductType,
      args: {
        code: { type: GraphQLNonNull(GraphQLID) }
      },
      resolve: product
    },
    order: {
      type: OrderType,
      args: {
        id: { type: GraphQLNonNull(GraphQLID) }
      },
      resolve: order
    },
    cart: {
      type: GraphQLNonNull(CartType),
      resolve: getCart
    }
  }
})

const mutationType = new GraphQLObjectType({
  name: 'Mutation',
  fields: {
    addProductToCart: {
      type: CartItemType,
      args: {
        productCode: { type: GraphQLNonNull(GraphQLID) }
      },
      resolve: addProductToCart
    },
    removeProductFromCart: {
      type: GraphQLBoolean,
      args: {
        productCode: { type: GraphQLNonNull(GraphQLID) }
      },
      resolve: removeProductFromCart
    },
    incrementCartItem: {
      type: GraphQLBoolean,
      args: {
        cartItemId: { type: GraphQLNonNull(GraphQLID) }
      },
      resolve: incrementCartItem
    },
    decrementCartItem: {
      type: GraphQLBoolean,
      args: {
        cartItemId: { type: GraphQLNonNull(GraphQLID) }
      },
      resolve: decrementCartItem
    },
    createSession: {
      type: GraphQLNonNull(SessionType),
      resolve: createSession
    },
    createProduct: {
      type: GraphQLNonNull(ProductType),
      args: {
        photo: { type: GraphQLUpload },
        name: { type: GraphQLNonNull(GraphQLString) },
        price: { type: GraphQLNonNull(GraphQLString) }
      },
      resolve: createProduct
    },
    createOrderFromCart: {
      type: GraphQLNonNull(OrderType),
      resolve: createOrderFromCart
    }
  }
})

const schema = new GraphQLSchema({
  query: queryType,
  mutation: mutationType
}) 

module.exports = schema