const Op = require('sequelize').Op
const numeral = require('numeral')
const addBusinessDays = require('date-fns/addBusinessDays')

let DB_PRICE_FORMAT = '0.0000'

async function searchProducts(_, {like}, req){
  if(like.length < 3){ return []}
  try{
    const db = req.db
  
    let products = await db.Product.findAll({
      where: {
        name: {
          [Op.like] : `%${like}%`
        }
      }
    })

    return products
  }
  catch(e){
    console.log(e)
  }

  return products
}

async function product(_, {code}, req) {
  const db = req.db
  let product = await db.Product.findByPk(code)

  return product.toJSON()
}

async function order(_, {id}, req){
  const db = req.db
  let order = await db.Order.findByPk(id)
  return order
}

async function addProductToCart(_, { productCode }, req){
  if(!req.session || !req.session.cartId){
    // return error
  }

  const { db } = req

  let cart = await db.Cart.findByPk(req.session.cartId)
  let product = await db.Product.findByPk(productCode)
  
  let cartItem = await db.CartItem.findOne({
    where: {
      cartId: cart.id,
      productCode
    }
  })

  if(cartItem){
    return cartItem
  }
  else{
    let cartItem = await db.CartItem.create({ quantity: 1, productCode })
    await cart.addCartItem(cartItem)
    return cartItem
  }
}

async function removeProductFromCart(_, { productCode }, req){
  const { db } = req

  let cart = await db.Cart.findByPk(req.session.cartId)
    
  let cartItem = await db.CartItem.findOne({
    where: {
      cartId: cart.id,
      productCode
    }
  })

  if(cartItem){
    await cartItem.destroy()
  }

  return true
}

async function incrementCartItem(_, { cartItemId }, req){
  const { db } = req

  let cartItem = await db.CartItem.findByPk(cartItemId)
  await cartItem.increment('quantity', {by: 1})
  return true
}

async function decrementCartItem(_, { cartItemId }, req){
  const { db } = req
  
  let cartItem = await db.CartItem.findByPk(cartItemId)
  if(cartItem.quantity === 1) {
    return false
  }

  await cartItem.decrement('quantity', {by: 1})
  return true
}

async function createSession(_, __, req){
  
  const db = req.db
  //Checks if the session is already stored
  let sessionExists = !!await new Promise((resolve,reject)=>{
    req.sessionStore.get(req.sessionID, (err, session)=>{
      if(err){
        reject(err)
      }else{
        resolve(session)
      }
    })
  })


  if(!sessionExists){
    req.sessionStore.generate(req)

    //saves the session to the database
    await new Promise((resolve,reject)=>{
      req.session.save((err)=>{
        if(err){
          reject(err)
        }else{
          resolve()
        }
      })
    })
    let cart = await db.Cart.create({ sessionId: req.sessionID })
    req.session.cartId = cart.id
  }

  return {
    sessionId: req.sessionID,
    cartId: req.session.cartId
  }
}

async function createProduct(_, { photo, price, name }, { db, cloudinary }){
  const { createReadStream } = await photo

  let { secure_url : photoUrl } = await new Promise((ok, fail) => {
    let stream = cloudinary.uploader.upload_stream((err, res) => { err ? fail(err) : ok(res)});
    createReadStream().pipe(stream)
  })
  
  let product = await db.Product.create({ photoUrl, price, name })
  return product
}

async function getCart(_, __, req ) {
  const db = req.db
  
  let cart = await db.Cart.findByPk(req.session.cartId, {
    include: [
      'cartItems'
    ]
  })

  return cart.toJSON()
}

async function createOrderFromCart(_, __, req){
  const db = req.db

  let cart = await db.Cart.findByPk(req.session.cartId, {
    include: [{
      model: db.CartItem,
      as: 'cartItems',
      include: ['product']
    }]
  })

  
  let subtotal = 0

  let orderItems = await db.OrderItem.bulkCreate(cart.cartItems.map(({ quantity, productCode, product })=>{
    let price = numeral(product.price).value()
    let orderItemTotal = price*quantity
    subtotal += orderItemTotal
    return {
      productCode,
      quantity,
      productPrice: product.price,
      productTotal: numeral(orderItemTotal).format(DB_PRICE_FORMAT)
    }
  }), { returning: true })

  let shipping = subtotal * 0.1

  let order = await db.Order.create({
    total: numeral(subtotal + shipping).format(DB_PRICE_FORMAT),
    taxes: numeral(subtotal*0.18).format(DB_PRICE_FORMAT),
    subtotal: numeral(subtotal).format(DB_PRICE_FORMAT),
    shipping: numeral(shipping).format(DB_PRICE_FORMAT),
    shippingDate: addBusinessDays(new Date(), 1),
    sessionId: req.sessionID
  })

  await order.setOrderItems(orderItems)
  await cart.setCartItems([])
  return order.toJSON()
}

module.exports = {
  searchProducts,
  product,
  order,
  createSession,
  addProductToCart,
  removeProductFromCart,
  createProduct,
  getCart,
  incrementCartItem,
  decrementCartItem,
  createOrderFromCart
}
