const createApp = require('./createApp')
const config = require('./config/config')
const db = require('./models')

let app = createApp({ db })

app.listen(config.get('app').port)