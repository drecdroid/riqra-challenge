const express = require('express')
const config = require('./config/config')
const graphqlHTTP = require('express-graphql')
const path = require('path')
const session = require('express-session')
const SequelizeStore = require('connect-session-sequelize')(session.Store);
const schema = require('./schema')
const { graphqlUploadExpress } = require('graphql-upload')

const cloudinary = require('cloudinary').v2
let cloudinaryConfig = config.get('cloudinary')

cloudinary.config({
  cloud_name: cloudinaryConfig.cloud_name,
  api_key: cloudinaryConfig.api_key,
  api_secret: cloudinaryConfig.secret_key
})


function createApp({ db }){

  let store = new SequelizeStore({
    db: db.sequelize,
    modelKey: 'Session',
    tableName: 'Sessions'
  })

  let app = express()
  let buildPath = path.join(__dirname, '..', 'client', 'build')
  
  app.use((req, res, next) => {
    req.cloudinary = cloudinary
    req.db = db
    next()
  })

  app.use(express.static(buildPath))
  
  app.use(session({
    secret: config.get('app').session_secret,
    store,
    resave: false,
    saveUninitialized: false
  }))

  app.use('/api/graphql',
    graphqlUploadExpress({ maxFileSize: 10000000, maxFiles: 1 }),
    graphqlHTTP({
      schema,
      graphiql: config.get('env') !== 'production'
    }
  ))


  app.get('/*', function (req, res) {
    res.sendFile(path.join(buildPath, 'index.html'))
  })

  return app
}

module.exports = createApp