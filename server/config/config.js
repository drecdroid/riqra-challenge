const convict = require('convict');
const cryptoRandomString = require('crypto-random-string');

if(process.env.USE_DOTENV){
  require('dotenv').config()
}

let config = convict({
  env: {
    format: ['production', 'development', 'test'],
    default: 'development',
    env: 'NODE_ENV'
  },
  db: {
    username: {
      default: '',
      env: 'DB_USERNAME',
      sensitive: true
    },
    password: {
      default: '',
      env: 'DB_PASSWORD',
      sensitive: true
    },
    name: {
      default: '',
      env: 'DB_NAME',
      sensitive: true
    },
    host: {
      default: '',
      env: 'DB_HOST',
      sensitive: true
    },
    port: {
      default: null,
      env: 'DB_PORT',
      sensitive: true
    }
  },
  app: {
    port: {
      format: 'port',
      default: 3000,
      env: 'PORT',
      arg: 'port'
    },
    session_secret: {
      default: cryptoRandomString({ length: 32 }),
      env: 'SESSION_SECRET',
      sensitive: true
    }
  },
  cloudinary: {
    cloud_name: {
      default: '',
      env: "CLOUDINARY_CLOUD_NAME"
    },
    api_key: {
      default: '',
      env: "CLOUDINARY_API_KEY"
    },
    secret_key: {
      default: '',
      env: "CLOUDINARY_API_SECRET",
      sensitive: true
    }
  }
})


module.exports = config