import gql from "graphql-tag";

export const FILTER_PRODUCTS = gql`
  query filterProducts($like: String!){
    filterProducts(like: $like){
      id
      name
      price
    }
  }
`

export const GET_PRODUCT = gql`
  query product($code : ID!){
    product(code : $code){
      name
      price
      photoUrl
      code
    }
  }
`


export const CREATE_PRODUCT = gql`
  mutation CreateProduct($photo: Upload!, $name: String!, $price: String!){
    createProduct(photo: $photo, name: $name, price: $price){
      code
    }
  }
`

export const ADD_PRODUCT_TO_CART = gql`
  mutation AddProductToCart($productCode : ID!){
    addProductToCart(productCode: $productCode){
      id
      productCode
      quantity
    }
  }
`

export const REMOVE_PRODUCT_FROM_CART = gql`
  mutation RemoveProductFromCart($productCode : ID!){
    removeProductFromCart(productCode: $productCode)
  }
`

export const INCREMENT_CART_ITEM = gql`
  mutation IncrementCartItem($cartItemId : ID!){
    incrementCartItem(cartItemId: $cartItemId)
  }
`

export const DECREMENT_CART_ITEM = gql`
  mutation DecrementCartItem($cartItemId : ID!){
    decrementCartItem(cartItemId: $cartItemId)
  }
`

export const GET_CART = gql`
  query GetCart {
    cart {
      id
      cartItems {
        id
        productCode
        quantity
      }
    }
  }
`

export const GET_CART_TOTAL = gql`{
  getCartTotal @client
}
`

export const CREATE_ORDER_FROM_CART = gql`
  mutation CreateOrderFromCart {
    createOrderFromCart {
      id
    }
  }
`