function once(fn, ...params){
  if(!fn.__executed){
    fn.call(null, ...params)
  }
  fn.__executed = true
}

export default once