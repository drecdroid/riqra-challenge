import React from 'react'
import styled from 'styled-components/macro'
import boxy from '../images/success.png'
import { H5Regular, Title, BigLink } from '../styledComponents'
import { Link, useLocation } from "@reach/router"

function ThankYou({ className }){
  let location = useLocation()
  let code = 'P' + location.state.orderId.toString().padStart(4, '0')

  return <div className={className}>
    <Title>Thank You</Title>
    <H5Regular>Your order <b>{code}</b> has been registered</H5Regular>
    <BigLink as={Link} to="/">Continue Shopping</BigLink>
    <img src={boxy} />
  </div>
}

export default styled(ThankYou)`
  display: flex;
  flex-direction: column;
  align-items: center;

  ${Title} {
    margin-bottom: 16px;
  }

  ${H5Regular} {
    margin-bottom: 16px;
  }

  ${BigLink} {
    margin-bottom: 48px;
  }

  img {
    max-width: 100%;
    width: 308px;
  }
`