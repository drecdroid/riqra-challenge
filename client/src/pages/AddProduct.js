import React, { useState, useCallback } from 'react'
import { TextInput, H4SemiBold, H5Regular, FormField, SubmitButton} from '../styledComponents'
import styled from 'styled-components/macro'
import ImageSelector from '../components/FileSelector'
import { useMutation } from '@apollo/react-hooks'
import { CREATE_PRODUCT } from '../queries'
import numeral from 'numeral'

// https://www.nnsoftware.at/post/input-field-type-currency-problem-solved

function AddProduct({ className }){
  let [ photo, setPhoto ] = useState(null)
  let [mutate] = useMutation(CREATE_PRODUCT)
  let [ price, setPrice ] = useState('')
  let [ type, setType ] = useState('text')
  let saveProduct = useCallback(async (e)=>{
    e.preventDefault()
    let formData = new FormData(e.target)
    
    let mutation = { variables: {
      photo,
      name: formData.get('name'),
      price: numeral(price).format('0.0000')
    }}
    
    await mutate(mutation)
    window.location.reload();
  }, [ photo, mutate, price])

  let priceChanges = useCallback((e)=>{
    setPrice(e.target.value)
  })

  let setNumber = useCallback((e)=>{
    setType('number')
    setPrice(numeral(price).value()||'')
  })

  let setText = useCallback((e)=>{
    setType('text')
    setPrice(numeral(price).format('$0,0.0000')||'')
  })

  return <form className={className} onSubmit={saveProduct}>
    <H4SemiBold>Add Product</H4SemiBold>
    
    <FormField>
      <H5Regular as="label">Photo</H5Regular>
      <ImageSelector onChange={setPhoto} />
    </FormField>

    <FormField>
      <H5Regular as="label">Name</H5Regular>
      <TextInput name="name" type="text" placeholder="Product Name" autoComplete="off"/>
    </FormField>

    <FormField>
      <H5Regular as="label">Price</H5Regular>
      <TextInput name="price" value={price} onChange={priceChanges} onFocus={setNumber} onBlur={setText} min="0" type={type} placeholder="$0.0000" autoComplete="off"/>
    </FormField>
    
    <SubmitButton as="input" type="submit" onChange={priceChanges} value="Add Product"/>
  </form>
}

export default styled(AddProduct)`
  width: 480px;

  & input[type=text], & input[type=number]{
    width: 100%;
  }
`