import React, { useCallback } from 'react'
import styled from 'styled-components/macro'
import { SubmitButton, DisabledSubmitButton, H5Regular, h5Regular, h5SemiBold } from '../styledComponents'
import CarIcon from '../components/CarIcon'
import ProductList from '../components/ProductList'
import SearchBox from '../components/SearchBox'

import { useQuery, useMutation } from '@apollo/react-hooks'
import { GET_CART, CREATE_ORDER_FROM_CART } from '../queries'
import addBusinessDays from 'date-fns/addBusinessDays'
import format from 'date-fns/format'
import PricingDetail from '../components/Pricing'
import { navigate } from '@reach/router'

function Home({className}){
  let { loading, error, data } = useQuery(GET_CART)

  let [ createOrderFromCart ] = useMutation(CREATE_ORDER_FROM_CART)

  let completeOrder = useCallback(async ()=>{
    let { data } = await createOrderFromCart({ refetchQueries: [{ query: GET_CART }]})
    
    navigate('/thank-you', { state : { orderId : data.createOrderFromCart.id }})
  })

  return <div className={className}>
    <div className="left-sidebar">
      <SearchBox />
      <ProductList />
    </div>

    <div className="right-sidebar">
      <div className="announce">
        <span className="car-icon">
          <CarIcon/>
        </span>
        <H5Regular>Buy now and get it by <b>{format(addBusinessDays(new Date(), 1), 'MM/dd/yy')}</b></H5Regular>
      </div>
      <div className="cart-detail">
        <PricingDetail />
      </div>

      { 
        loading || ( data && !data.cart.cartItems.length) ?
          <DisabledSubmitButton>Complete Order</DisabledSubmitButton> :
          <SubmitButton onClick={completeOrder}>Complete Order</SubmitButton>
      }
    </div>
  </div>
}


export default styled(Home)`
  display:flex;
  flex-direction: row;
  flex-wrap: wrap;
  
  ${SearchBox}{
    margin-bottom: 16px;
  }

  .announce {
    display: flex;
    flex-wrap: nowrap;
    justify-content: center;

    .car-icon {
      margin-right: 12px
    }

    margin-bottom: 16px;
  }

  .left-sidebar {
    max-width: 100%;
    width: 360px;
    margin-right: 64px;

    display: flex;
    flex-direction: column;
  }

  .right-sidebar {
    max-width: 100%;
    width: 360px;
    margin-top: 64px;

    .cart-detail {
      height: 160px;
      background: white;
      margin-bottom: 16px;
      padding: 16px;
      box-sizing: border-box;
    }
  }


  @media (max-width: 820px) {
    flex-direction: column;
    
    .left-sidebar{
      margin-right: 0px;
    }
  }
`