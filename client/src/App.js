import React, { useState, useEffect } from 'react';
import './App.css';
import { ThemeProvider } from 'styled-components'
import { ApolloClient } from 'apollo-client';
import { ApolloProvider } from '@apollo/react-hooks'
import { InMemoryCache } from 'apollo-cache-inmemory';
import { createUploadLink } from 'apollo-upload-client';
import { Router } from "@reach/router"
import AddProduct from './pages/AddProduct';
import ThankYou from './pages/ThankYou';
import Home from './pages/Home';
import styled from 'styled-components/macro'
import gql from 'graphql-tag'

const cache = new InMemoryCache({
  dataIdFromObject(object){
    switch(object.__typename){
      case 'Session':
        return object.sessionId
      case 'Product':
        return object.code
      default:
        return object.id
    }
  }
})


const client = new ApolloClient({
  cache,
  link: createUploadLink({
    uri: '/api/graphql'
  }),
  resolvers: {
    Query: {
      getCartTotal(_root, _, { cache }){
        let { cartId } = cache.readQuery({ query: gql`{ cartId @client }`})
        let { cartItems } = cache.readFragment({
          fragment: gql`
            fragment cart on Cart {
              cartItems {
                productCode
                quantity
              }
            }
          `,
          id: cartId
        })

        return cartItems.reduce((acc, cartItem) => {
          let product = cache.readFragment({ 
            fragment: gql`
              fragment product on Product {
                code                
                price
              }
            `,
            id: cartItem.productCode
          })
          return acc + Number.parseFloat(product.price) * cartItem.quantity
        }, 0)
      }
    }
  }
})


const theme = {
  accent: "#FF8000",
  text: "#333333",
  textLight: "#C1C1C1",
  grayBorder: "#DDDDDD",
  link: "#0500FF",
  important: "#FF2D55",
}

let StyledRouter = styled(Router)`
  display: flex;
  min-height: 100vh;
  justify-content: center;
  align-items: center;
  width: 100%;
  padding: 16px;
  box-sizing: border-box;

  & > * {
    max-width: 100%
  }
`

function App() {
  let [hasSession, setHasSession] = useState(false)
  useEffect(()=>{
    (async()=>{
      let {data} = await client.mutate({
        mutation: gql`mutation CreateSession {
          createSession {
            sessionId,
            cartId
          }
        }`
      })
      

      cache.writeData({
        data: {
          sessionId: data.createSession.sessionId,
          cartId: data.createSession.cartId,
        }
      })

      if(data.createSession){
        setHasSession(true)
      }
    })()
  },[])
  
  
  return (
    hasSession ? 
      <ThemeProvider theme={theme} >
        <ApolloProvider client={client}>
          <StyledRouter>
            <Home path="/" />
            <AddProduct path="/add-product"/>
            <ThankYou path="/thank-you"/>
          </StyledRouter>
        </ApolloProvider>
      </ThemeProvider> : null
  );
}

export default App;
