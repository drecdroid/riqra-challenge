import React, { useCallback } from 'react'
import styled from 'styled-components/macro'
import { useQuery, useMutation } from "@apollo/react-hooks";
import basquet from '../images/basquet.png'
import { H4SemiBold, H5Regular, h5SemiBold, h6Regular } from "../styledComponents";
import { GET_CART, GET_PRODUCT, REMOVE_PRODUCT_FROM_CART, INCREMENT_CART_ITEM, DECREMENT_CART_ITEM, GET_CART_TOTAL } from '../queries';
import BeatLoader from "react-spinners/BeatLoader";
import MinusIcon from '../images/minus-icon.svg';
import PlusIcon from '../images/plus-icon.svg';
import numeral from 'numeral'

function EmptyCart({ className }){
  return <div className={className}>
    <div className="wrapped">
      <img src={basquet} />
      <H4SemiBold>Your cart is empty</H4SemiBold>
      <H5Regular>Seems like you haven’t chosen what to buy...</H5Regular>
    </div>
  </div>
}

EmptyCart = styled(EmptyCart)`
  display: flex;
  flex-direction: column;
  height: 100%;
  padding: 60px;
  box-sizing: border-box;
  align-items: center;
  justify-content: center;

  .wrapped {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    width: 227px;
  }

  img {
    margin-bottom: 16px;
  }

  ${H4SemiBold}  {
    margin-bottom: 8px;
  }

  ${H5Regular}{
    text-align: center;
  }
`

let refetchQueries = [{ query: GET_CART}]

function CartItem({ id, quantity, productCode, className }) {
  let { loading, error, data } = useQuery(GET_PRODUCT, { variables: { code: productCode }})
  let [ removeProductFromCart ] = useMutation(REMOVE_PRODUCT_FROM_CART)
  let [ incrementCartItem ] = useMutation(INCREMENT_CART_ITEM)
  let [ decrementCartItem ] = useMutation(DECREMENT_CART_ITEM)

  const onRemove = useCallback(()=>{
    removeProductFromCart({
      variables: { productCode },
      refetchQueries
    })
  })

  const onIncrement = useCallback(()=>{
    incrementCartItem({
      variables: { cartItemId: id },
      refetchQueries
    })
  })

  const onDecrement = useCallback(()=>{
    decrementCartItem({
      variables: { cartItemId: id },
      refetchQueries
    })
  })

  let content 
  if(loading){
    content = <div className="loading-box"><BeatLoader color="gray" /></div>
  }
  else if(error){
    console.log(error)
    content = <div>error</div>
  }else{
    content = <>
      <div className="image-box">
        <img src={data.product.photoUrl} />
      </div>
      <div className="detail-box">
        <H5Regular>{data.product.name}</H5Regular>
        <H4SemiBold>{numeral(data.product.price).format('$0,0.00')}</H4SemiBold>
      </div>
      <div className="controls-box">
        <div className="quantity-controls">
          <button onClick={onDecrement}><img src={MinusIcon} /></button>
          <span className="quantity">{quantity}</span>
          <button onClick={onIncrement}><img src={PlusIcon} /></button>
        </div>
        <button className="remove-button" onClick={onRemove}>remove</button>
      </div>
    </>
  }

  return <div className={className}>
    {content}
  </div>
}


CartItem = styled(CartItem)`
  height: 106px;
  display: flex;
  padding: 16px;
  box-sizing: border-box;
  flex-direction: row;
  
  & .loading-box {
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
  }

  & .image-box {
    max-width: 80px;
    width: 80px;
    display: flex;
    justify-content: center;
    margin-right: 16px;

    & img {
      height: 100%;
      width: auto;
    }
  }

  & .detail-box {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
  }

  & .controls-box {
    display: flex;
    align-items: center;
    flex-direction: column;
    justify-content: space-between;
    margin-left: auto;  
  }

  & .quantity-controls {
    display: flex;
    flex-direction: row;
    max-width: 100px;
    width: 100px;
    justify-content: space-between;
    background-color: ${p => p.theme.accent};
    padding: 8px;
    box-sizing: border-box;
    border-radius: 5px;

    & .quantity {
      ${h5SemiBold}
      color: white;
    }

    & button {
      background: none;
      border:none;
      padding: 0;
    }
  }

  & .remove-button {
    ${h6Regular}
    background: none;
    border: none;
  }

  ${H4SemiBold} {
    color: ${p => p.theme.important}
  }
`

function ProductList({ className }){
  let { loading, error, data } = useQuery(GET_CART)

  let content
  if(loading){
    content = <></>
  }else if( !data.cart.cartItems.length ){
    content = <EmptyCart />
  }else{
    content = data.cart.cartItems.map(cartItem => {
      return <CartItem {...cartItem} key={cartItem.id} />
    })
  }

  return <div className={className}>
    {content}
  </div>
}

export default styled(ProductList)`
  height: 480px;
  overflow: auto;
  background: white;

  ${CartItem} {
    border-bottom: 1px solid ${p => p.theme.grayBorder};
  }
`