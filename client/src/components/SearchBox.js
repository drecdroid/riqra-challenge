import React, { useCallback, useRef, useEffect, useState } from 'react'
import gql from 'graphql-tag'
import { useLazyQuery, useMutation } from '@apollo/react-hooks'
import { useDebouncedCallback } from 'use-debounce'
import { TextInput, grayBorder, h5SemiBold } from '../styledComponents'
import styled from 'styled-components/macro'
import { StyledPriceRow } from './PricedRow'
import { ADD_PRODUCT_TO_CART, GET_CART, GET_CART_TOTAL } from '../queries'
import numeral from 'numeral'

const SEARCH_PRODUCTS = gql`
  query SearchProducts( $like : String! ) {
    searchProducts( like: $like ) {
      code
      name
      price
    }
  }
`

function SearchBox({ className }){

  const [ searchProducts, { data } ] = useLazyQuery(SEARCH_PRODUCTS)
  const [ inputText, setInputText ] = useState('')
  const [ addProductToCart ] = useMutation(ADD_PRODUCT_TO_CART)  

  const [ debouncedSearch ] = useDebouncedCallback((like)=>{
    searchProducts({ variables: { like } })
  }, 500)

  useEffect(()=>{
    debouncedSearch(inputText)
  }, [inputText])

  const selectProduct = useCallback((productCode)=>{
    addProductToCart({
      variables: { productCode },
      refetchQueries: [{ query: GET_CART}]
    })
    setInputText('')
  })

  return <div className={className}>
    <TextInput
      name="search"
      type="text"
      value={inputText}
      placeholder="Search Products"
      autoComplete="off"
      onChange={e => setInputText(e.target.value)}
    />
    {
      data && data.searchProducts && data.searchProducts.length ?
      <div className="search-results">
        { data.searchProducts.map( product => {
          return <StyledPriceRow
            key={product.code}
            label={product.name}
            price={numeral(product.price).format('$0,0.00')}
            onClick={()=> selectProduct(product.code)}
          />
        }) }
      </div> : null
    }
  </div>
}

export default styled(SearchBox)`
  ${grayBorder}
  position: relative;
  width: 100%;

  ${TextInput}{
    width: 100%;
  }

  & .search-results {
    ${grayBorder}
    ${h5SemiBold}
    position:absolute;
    top: 100%;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
    left:0px;
    width: 100%;
    background: white;
    box-sizing: border-box

    
  }

  & ${StyledPriceRow}{
    margin-bottom: 0;
    padding: 12px 16px;
    cursor: pointer; 

    &:not(:last-child){  
      border-bottom: 1px solid ${p => p.theme.grayBorder};
    }

    &:hover {
      background-color: ${p => p.theme.grayBorder}
    }
  }
`