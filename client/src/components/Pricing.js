import React from 'react'
import { StyledPriceRow, TotalRow, ShippingRow } from '../components/PricedRow'
import { useQuery } from '@apollo/react-hooks'
import { GET_CART_TOTAL } from '../queries'
import numeral from 'numeral'

let format = '$0,0.00'

function PricingDetail(){
  let { loading, error, data } = useQuery(GET_CART_TOTAL, { pollInterval: 200 })
  
  let products = numeral(loading ? 0 : data && data.getCartTotal || 0).value()
  let shipping = numeral(products * 0.1).value()
  let taxes = numeral(products * 0.18).value()
  let total = numeral(products + shipping).value()

  return <>
    <StyledPriceRow label="Products" price={numeral(products).format(format)} />
    <ShippingRow label="Shipping Cost" price={numeral(shipping).format(format)}/>
    <StyledPriceRow label="Taxes" price={numeral(taxes).format(format)}/>
    <TotalRow label="Total" price={numeral(total).format(format)}/>
  </>
}


export default PricingDetail