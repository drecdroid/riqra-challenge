import React from 'react'
import { useRef, useState, useEffect, useCallback } from "react"
import subir from '../images/subir.png'
import styled from 'styled-components/macro'
import { h5SemiBold, grayBorder, H5Regular } from "../styledComponents"

const FileInput = styled.input`
  width: 0.1px;
  height: 0.1px;
  opacity: 0;
  overflow: hidden;
  position: absolute;
  z-index: -1;
  
  & + label {
    ${h5SemiBold}
    ${grayBorder}
    padding: 4px 16px;
    color: white;
    background-color: ${props => props.theme.accent};
    display: inline-block;
  }

  &:focus + label,
  & + label:hover {
    box-shadow: ${props => props.theme.accent} 0px 0px 2px;
    cursor: pointer;
  }
`

const ImagePlaceholder = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  border-width: 2px;
  border-style: dashed;
  border-color: #000;
  height: 250px;
  justify-content: center;
  align-items: center;
  margin-bottom: 8px;
  cursor: pointer;
`

function removeDragData(e) {
  if (e.dataTransfer.items) {
    e.dataTransfer.items.clear();
  } else {
    e.dataTransfer.clearData();
  }
}

function ImageSelector({ onChange, className}){
  onChange = onChange || function(){};
  let ref = useRef(null)
  let fileReader = useRef(new FileReader())
  let [ imagePreview, setImagePreview ] = useState(null)

  useEffect(()=>{
    let listener = ()=>{
      setImagePreview(fileReader.current.result)
    }

    fileReader.current.addEventListener('loadend', listener)

    return () => {
      fileReader.current.removeEventListener('loadend', listener)
    }

  }, [onChange])

  let onFileSelect = useCallback((e)=>{
    let file = e.target.files[0]
    if(file){
      fileReader.current.readAsDataURL(file)
      onChange(file)
    }else{
      setImagePreview(null)
      onChange(null)
    }
  }, [onChange])

  let dropHandler = useCallback((e) => {
    e.preventDefault();
    let items = e.dataTransfer.files || e.dataTransfer.items

    if (items.length === 1 && ([undefined, 'file'].includes(items[0].kind))) {
      let file = items[0] 
      fileReader.current.readAsDataURL(file)
      onChange(file)
    } 
    removeDragData(e)
  }, [onChange])

  let deactiveDragOver = useCallback((e)=>{
    e.preventDefault();
  })

  return <div className={className}>
    { imagePreview ?
      <img onDrop={dropHandler} onDragOver={deactiveDragOver} src={imagePreview} /> :
      <ImagePlaceholder onDrop={dropHandler} onDragOver={deactiveDragOver} onClick={() => ref.current.click()}>
        <img src={subir} />
        <H5Regular>Drop product photo here.</H5Regular>
      </ImagePlaceholder>  
    }
    <FileInput type="file" id="file" ref={ref} accept="image/*" onChange={onFileSelect}/>
    <label htmlFor="file">Choose File</label>
  </div>
}

ImageSelector = styled(ImageSelector)`
  display: flex;
  flex-direction: column;
  align-items: center;

  img {
    margin-bottom: 8px;
    max-width: 100%;
  }

  ${FileInput} + label {
    flex-grow: 0;
  }
`

export default ImageSelector