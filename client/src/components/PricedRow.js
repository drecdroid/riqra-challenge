import React from 'react'
import { h5Regular, h5SemiBold } from '../styledComponents'
import styled from 'styled-components/macro'

function PriceRow({ className, label, price, ...props }){
  return <div className={className} {...props}>
    <span className="label">{label}</span>
    <span className="price">{price}</span>
  </div>
}

let StyledPriceRow = styled(PriceRow)`
  ${h5Regular}
  display: flex;
  flex-wrap: nowrap;
  margin-bottom: 8px;

  .label {
    margin-right: auto;
  }
`

let ShippingRow = styled(StyledPriceRow)`
  background: #FFE200;
`

let TotalRow = styled(StyledPriceRow)`
  ${h5SemiBold}
  margin-bottom: 0px;
  margin-top: 16px;

  .price {
    color: #FF2D55;
  }
`

export {
  PriceRow,
  StyledPriceRow,
  ShippingRow,
  TotalRow
}