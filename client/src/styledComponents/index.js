import styled, { css } from 'styled-components/macro'

export const commonTextStyle = `
  font-family: Open Sans;
  font-style: normal;
`

export const semiBold = `
  font-weight: 600;
`

export const grayBorder = css`
  border-width: 1px;
  border-style: solid;
  box-sizing: border-box;
  border-radius: 4px;
  border-color: ${props => props.theme.grayBorder};
`

export const blackText = css`
  color: ${props => props.theme.text};
`

export const lighText = css`
  color: ${props => props.theme.textLight};
`

export const commonH5Style = css`
  ${commonTextStyle}
  font-size: 16px;
  line-height: 24px;
`

export const h6Regular = css`
  ${commonTextStyle}
  font-weight: normal;
  font-size: 12px;
  line-height: 18px;
`

export const h5Regular = css`
  ${commonH5Style}
  font-weight: normal;
`

export const h5SemiBold = css`
  ${commonH5Style}
  ${semiBold}
`

export const h4SemiBold = css`
  ${commonTextStyle}
  ${semiBold}
  font-size: 21px;
  line-height: 32px;
`

export const textInput = css`
  padding: 12px 16px;
  ${grayBorder}
  ${h5Regular}
  ${blackText}
`


export const FormField = styled.div`
  margin-bottom: 16px;

  & label:first-child{
    display: block;
    margin-bottom: 8px;
  }
`

export const submitButton = css`
  padding: 13px 16px 11px 16px;
  background-color: ${p => p.theme.accent};
  ${h5SemiBold}
  text-align: center;
  border: none;
  width: 100%;
`

export const SubmitButton = styled.button`
  ${submitButton}
  color: white;
  border: none;
  cursor: pointer;
`

export const DisabledSubmitButton = styled.button.attrs(p =>({ disabled: true }))`
  ${submitButton}
  ${grayBorder}
  background: none;
  color: ${p => p.theme.textLight}
`

export const Title = styled.h2`
  ${commonTextStyle}
  ${blackText}
  ${semiBold}
  font-size: 28px;
  line-height: 42px;
`

export const BigLink = styled.a`
  ${h5Regular}
  text-decoration: none;
  color: ${p => p.theme.link}
`

export const H5Regular = styled.h5`${h5Regular}`
export const H5SemiBold = styled.h5`${h5SemiBold}`
export const H4SemiBold = styled.h4`${h4SemiBold}`
export const TextInput = styled.input`${textInput}`